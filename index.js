// Parametri
require("./parametri.js")
// Fine parametri

let http = require('http');
let fs = require('fs');
let url = require("url");
let path = require("path");
let BigDownloader = require("bigdownloader")

global.daScaricare = []
global.totaleElementi = 0
let urlLista = "https://apps.jw.org/GETPUBMEDIALINKS?output=json&pub=" + pubblicazione + "&fileformat=" + formato + "&alllangs=1&langwritten=" + carattereLingua + "&txtCMSLang=" + carattereLingua
console.log("Link: " + urlLista)

// Crea cartelle
if (!fs.existsSync(".\\output"))
    fs.mkdirSync(".\\output")
if (!fs.existsSync(".\\output\\completati"))
    fs.mkdirSync(".\\output\\completati")


require("request")(urlLista, function (error, response, body) {
	
	
	console.log('error:', error)
	console.log('statusCode:', response && response.statusCode)
	console.log(JSON.parse(body).pubName)
	let tuttiFile = JSON.parse(body).files[carattereLingua][formato]
	daScaricare = tuttiFile.filter(file => file.label == risoluzione)
	totaleElementi = daScaricare.length
	console.log("Elementi totali: " + totaleElementi)

	scarica()
	
})

function scarica() {
	
	let e = daScaricare[0]
	console.log("------------------------------------------------------------------")
	console.log("Scarico elemento #" + (totaleElementi - daScaricare.length + 1) + " (" + e.file.url + ")")
	
	// Scarica (se non c'è già) in "output"
	let nomeFile = path.basename(e.file.url)
	if (!fs.existsSync(".\\output\\completati\\" + nomeFile)) {
		// Scarica
		let downloader = new BigDownloader({
			url		: e.file.url.replace("https","http"),
			path	: ".\\output\\" + nomeFile
		})
		downloader.on("progress", function(progress) {
			// Avanzamento
			process.stdout.write((progress.percent * 100).toFixed(2) + "% di " + (this.size / 1000000).toFixed(1) +  " MB     \r")
		}).on("finish", function() {  
			console.log("[Scaricato]                        ")
			// File scaricato, lo sposta in "output\completati"
			fs.rename(downloader.opts.path, downloader.opts.path.replace("output", "output\\completati"))
			// Lo toglie dall'array
			daScaricare.shift()
			if (daScaricare.length > 0)
				scarica()
		}).start()
	} else {
		// Già scaricato
		console.log("[Già scaricato]")
		daScaricare.shift()
		if (daScaricare.length > 0)
			scarica()
	}
	
}
